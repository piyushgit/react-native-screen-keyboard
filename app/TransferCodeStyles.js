export default {
    container:{
        flexDirection: 'column'
    },
    textInputWrapper: {
        paddingVertical: 20,
        flex: 1,
        alignItems: 'center'
    },
    dismissWrapper:{
        padding: 30,
        alignItems: 'flex-end'
    },
    dismiss:{
        padding: 7
    },
    headerWrapper:{
        paddingTop: 30, 
        paddingLeft: 20,
    },
    header:{
        color: '#fff',
        fontSize: 28,
        fontWeight: 'bold'
    }

}