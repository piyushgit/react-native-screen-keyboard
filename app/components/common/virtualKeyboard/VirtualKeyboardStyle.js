import { StyleSheet, Dimensions, Platform } from 'react-native';
const { height, width } = Dimensions.get('window');

export default {
	container: {
		flexDirection: 'column',
		justifyContent: 'center',
		alignItems: 'center'
	},
	content: {
		marginTop: 20,
		marginLeft: 30,
		marginRight: 30,
		alignItems: 'flex-start',
	},
	row: {
		flexDirection: 'row',
		width: 275,
		marginTop: 15,
	},
	number: {
		fontSize: 25,
		textAlign: 'center',
		padding: 15
	},
	backspace: {
		flex: 1,
		alignItems: 'center',
		justifyContent: 'center'
	},
	cell: {
		borderWidth: 1,
		borderColor: '#fff',
		marginHorizontal: 10,
		width: 70,
		height: 70,
        borderRadius: 150/2,
	},
	codeBoxWrapper: {
		flex: -1,
		flexDirection: 'row',
		justifyContent: 'space-between',
		width: 250,
	},
	codeBox: {
		borderBottomWidth: 1,
		borderBottomColor: '#ffffff',
		height: 30,
		width: 30,
		marginRight: 14,
		alignItems: 'center',
		justifyContent: 'center',
	},
	codeBoxText: {
		color: '#fff',
		fontSize: 20,
		fontWeight: 'bold'
	}
}