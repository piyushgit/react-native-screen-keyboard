import React, {Component} from 'react';
import {View, Text} from 'react-native';

import styles from './VirtualKeyboardStyle';

export class VirtualKeyboardTextDisplay extends Component {

    codeBox = (key,  hasValue) => {
        var codeString = this.props.pinValue;
        var code = codeString!=null?codeString:[]; 
        return (
            <View
                key={key}
                style={styles.codeBox}>
                {hasValue?<Text style={styles.codeBoxText}>{code[key-1]}</Text>:<Text style={styles.codeBoxText}>{'*'}</Text>}
            </View>
        );
    }

    render() {
        return (
        <View 
            style={styles.codeBoxWrapper}>
            {this.renderCodes()}
        </View>
        );
    }

    renderCodes() {
        let codes = [];

        for (let i = 0; i < this.props.pinLength; i++) {
            codes.push(this.renderCode(i + 1));
        }
        return codes;
    }

    renderCode(index) {
        hasValue=this.props.pinValue && this.props.pinValue.length >= index;
        return this.codeBox(index, hasValue);
    }
}