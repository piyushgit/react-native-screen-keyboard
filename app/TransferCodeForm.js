import React, {Component} from 'react';
import {View, Image, Text, TouchableOpacity, Dimensions} from 'react-native';
import LinearGradient from 'react-native-linear-gradient';

import Platform from './components/common/helper/platform';
import VirtualKeyboard from "./components/common/virtualKeyboard/VirtualKeyboard";
import {VirtualKeyboardTextDisplay} from "./components/common/virtualKeyboard/VirtualKeyboardTextDisplay";
import TransferCodeStyles from "./TransferCodeStyles";

class TransferCodeForm extends Component {

	constructor(props) {
		super(props);
		this.state = {
			code: null,
		};

		this.pinLength = 7;
	}

	onPressDismiss = () => {
		this.props.navigation.goBack();
	}

	render() {
		console.log(Platform.isPortrait() );
		return (
			<LinearGradient colors={['#4c669f', '#3b5998', '#192f6a']} style={{height: '100%'}}>
			<View style={[TransferCodeStyles.container]}>
				<View style={TransferCodeStyles.textInputWrapper}>
					<View style={{width: this.pinLength>6?Platform.isPortrait()?'80%':'50%':null}}>
					<VirtualKeyboardTextDisplay pinLength={this.pinLength} pinValue={this.state.code} />
					</View>
				</View>
				<VirtualKeyboard color='#fff' pinLength={this.pinLength} onPress={(val) => this.changeText(val)} />
			</View>
			</LinearGradient>
		);
	}

	changeText(newCode) {
		const {dispatch} = this.props;
		let code = newCode.split('');
		if (code.length == this.pinLength )
		{
			this.setState({code: code});
		}	
		else
		{
			this.setState({code: code});
		}		
	}

}

export default TransferCodeForm;